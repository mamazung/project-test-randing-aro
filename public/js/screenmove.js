var $screen_animation = $('.screen_element');
var $screen_animation_boom = $('.screen_vga_boom');
var $screen_animation_after = $('.screen_element_after');
var $screen_animation_boom_back = $('.screen_element_boom_back');
var $screen_animation_boom_wing = $('.screen_element_boom_wing');
var $window = $(window.parent);




function check_if_in_view_screen() {




    if ($(window.parent.document).find("#iframe_kf").is(':visible') || $(window.parent.document).find("#kf-iframe").is(':visible')) {


    var window_height = $window.height();
    var window_top_position = $window.scrollTop();
    var window_bottom_position = (window_top_position + window_height);
    var kf_height_obj = $(window.parent.document).find('#iframe_kf') || $(window.parent.document).find('#kf-iframe');
    var kf_height = (kf_height_obj.length > 0) ? kf_height_obj.offset().top : 0;
    var kf_height_respond_obj = $(window.parent.document).find('#iframe_kf_respond') || $(window.parent.document).find('#kf-iframe_respond');
    var kf_height_respond = (kf_height_respond_obj.length > 0) ? kf_height_respond_obj.offset().top : 0;



        $.each($screen_animation, function() {
            var $element = $('.screen_element');
            var element_height = $element.outerHeight();
            var element_top_position = $element.offset().top;
            var real_element_top_position = (element_top_position + kf_height)
            var element_bottom_position = (element_top_position + element_height);

            //check to see if this current container is within viewport
            if ((element_bottom_position >= window_top_position) &&
                (real_element_top_position <= window_bottom_position)) {
                setTimeout(function() {

                    $('.handle').addClass('handle_0');

                    $('.resize').addClass('resize_0');
                }, 500);



            } else {

            }
        });


        $.each($screen_animation_boom, function() {
            var $element_boom = $('.screen_vga_boom');
            var element_height_boom = $element_boom.outerHeight();
            var element_top_position_boom = $element_boom.offset().top + 100;
            var real_element_top_position_boom = (element_top_position_boom + kf_height)
            var element_bottom_position_boom = (element_top_position_boom + element_height_boom);

            //check to see if this current container is within viewport
            if ((element_bottom_position_boom >= window_top_position) &&
                (real_element_top_position_boom <= window_bottom_position)) {

                $('.vga_booom_word').css({ "opacity":"1", "transition": "2s" });

                $('.vga_booom_01').css({ "left": "0", "transition": "2s" });

                $('.vga_booom_02').css({ "left": "0", "transition": "2s" });

                $('.vga_booom_03').css({ "left": "0", "transition": "2s" });

                $('.vga_booom_04').css({ "left": "0", "transition": "2s" });

                $('.vga_booom_05').css({ "left": "0", "transition": "2s" });

                $('.vga_booom_06').css({ "left": "0", "transition": "2s" });

                

            } else {


                $('.vga_booom_word').css({ "opacity":"0", "transition": "2s" });

                $('.vga_booom_01').css({ "left": "12.5%", "transition": "1.5s" });

                $('.vga_booom_02').css({ "left": "-22%", "transition": "1.5s" });

                $('.vga_booom_03').css({ "left": "-10%", "transition": "1.5s" });

                $('.vga_booom_04').css({ "left": "3.5%", "transition": "1.5s" });

                $('.vga_booom_05').css({ "left": "12.5%", "transition": "1.5s" });

                $('.vga_booom_06').css({ "left": "23%", "transition": "1.5s" });

            }
        });


             $.each($screen_animation_boom_back, function() {
            var $element_boom_back = $('.screen_element_boom_back');
            var element_height_boom_back = $element_boom_back.outerHeight();
            var element_top_position_boom_back = $element_boom_back.offset().top + 700;
            var real_element_top_position_boom_back = (element_top_position_boom_back + kf_height)
            var element_bottom_position_boom_back = (element_top_position_boom_back + element_height_boom_back);

            //check to see if this current container is within viewport
            if ((element_bottom_position_boom_back >= window_top_position) &&
                (real_element_top_position_boom_back <= window_bottom_position)) {

                $('#aorus_back_left').css({ "animation":"aorus_back_left ease-in-out 6s infinite" });

                $('#aorus_back_light').css({ "animation":"aorus_back_light ease-in-out 6s infinite" });

               

            } else {


               

            }
        });





        $.each($screen_animation_after, function() {
            var $element_after = $('.screen_element_after');
            var element_height_after = $element_after.outerHeight();
            var element_top_position_after = $element_after.offset().top;
            var real_element_top_position_after = (element_top_position_after + kf_height)
            var element_bottom_position_after = (element_top_position_after + element_height_after);

            //check to see if this current container is within viewport
            if (real_element_top_position_after <= window_bottom_position) {

                $("#page1").slideUp("slow");
                $("#page2").slideDown("slow");


            } else {


                $("#page2").slideUp("slow");
                $("#page1").slideDown("slow");


            }
        });





    } else {

        $.each($screen_animation, function() {
            var $element = $('.screen_element');
            //check to see if this current container is within viewport

            setTimeout(function() {

                $('.handle').addClass('handle_0');

                $('.resize').addClass('resize_0');
            }, 500);



        });

    }









}

$window.on('scroll resize', check_if_in_view_screen);
$window.trigger('scroll');