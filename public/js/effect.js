$(document).ready(function () {
    //ui mousemove
    window.onload = function () {
        let kv2 = document.querySelector('#kv2'),
            // elemOne = document.querySelector('#ui1'),
            // elemTwo = document.querySelector('#ui2'),
            elemThree = document.querySelector('#ui3'),
            elemFour = document.querySelector('#ui4');

        kv2.addEventListener('mousemove', function (e) {
            var pageX = e.clientX - window.innerWidth / 2,
                pageY = e.clientY - window.innerHeight / 2;
            // elemOne.style.transform = 'translateX(' + (5 + pageX / 500) + '%) translateY(' + (5 + pageY / 1000) + '%)';
            // elemTwo.style.transform = 'translateX(' + (1 + pageY / 1000) + '%) translateY(-' + (1 + pageX / 5000) + '%)';
            elemThree.style.transform = 'translateX(' + (2 - pageX / 1500) + '%) translateY(' + (1 - pageX / 3000) + '%)';
            elemFour.style.transform = 'translateX(-' + (1 + pageX / 3000) + '%) translateY(-' + (1 - pageY / 2000) + '%)';
        });
    }
    // aorus slider

    var movementStrength = 20;
    var height = movementStrength / $(window.parent).height();
    var width = movementStrength / $(window.parent).width();

    $("#top-image_aorus").mousemove(function (e) {
        var pageX = e.clientX;
        var pageY = e.clientY;
        var newvalueX = width * pageX;
        var newvalueX_01 = +newvalueX;
        var newvalueY = height * pageY;
        var newvalueY_01 = +10;
        $('#top-image_aorus').css("background-position", newvalueX_01 + "%     " + newvalueY_01 + "%");
    });

    var $window = $(window.parent);

    function check_vr_aorus() {
        var bg_width = $(window.parent).width();
        var bg_width_real = bg_width * 1.06;
        $('#top-image_aorus').css("background-size", bg_width_real + "px     ");
    };

    $window.on('scroll resize', check_vr_aorus);
    $window.trigger('scroll');



    // gallery

    var photo_item = $('.mouse-gallery .mouse-gallery-item .gallery-jpg'),
        mouse_photo = $('.gallery-big-jpg'),
        degree = $('.degree-watch'),
        degree_s = $('.gallery-degree');

    degree.css('opacity', '0');
    $('#H5_scroll').addClass('cursor-_auto');
    photo_item.click(function () {
        degree.css('opacity', '0');
        mouse_photo.show();
        var self = $(this),
            index = self.index();
        degree_s.removeClass('photo-active');
        photo_item.removeClass('photo-active');
        photo_item.eq(index).addClass('photo-active');
        var src_img = "img/photo" + "/0" + (index + 1) + ".png";
        $('#H5_scroll').addClass('cursor-_auto');
        mouse_photo.attr('src', src_img);
    });

    // degree_s.click(function () {
    //     photo_item.removeClass('photo-active');
    //     degree_s.addClass('photo-active');
    //     mouse_photo.hide();
    //     $('#H5_scroll').removeClass('cursor-_auto');
    //     degree.css('opacity', '1');
    // });





    // change color

    $(".colorwell").hide();
    $(".rainbow_es").hide();
    // $(".color-piceker").hide();
    $(".change_color_keep").hide();



    $(".p2").hide();
    $(".p3").hide();
    $(".p4").hide();
    $(".p1").hide();
    $(".p7").hide();
    // $(".p0").hide();
    $(".pes").hide();
    $(".p8").hide();
    $(".p9").hide();
    $(".p10").hide();
    $(".p11").hide();
    $(".p12").hide();
    $(".p13").hide();

    $(".fade_in_speed").hide();



    $(".btn1").click(function () {
        // $(".color-piceker").show();
        $(".fuck").hide();
        $(".rainbow_es").hide();
        $(".colorwell").show();
        $(".colorwell").removeClass('shite1');
        $(".colorwell").removeClass('shite2');
        $(".colorwell").removeClass('shite3');
        $(".colorwell").removeClass('shite4');
        $(".colorwell").addClass('shite');
        $(".vga_fan_light").show();
        $(".vga_fan_light").addClass('shite');
        $(".vga_fan_light").removeClass('shite1');
        $(".vga_fan_light").removeClass('shite3');
        $(".vga_fan_light").removeClass('shite2');
        $(".p2").hide();
        $(".p3").hide();
        $(".p4").hide();
        $(".p0").hide();
        $(".pes").hide();
        $(".p1").show();
        $(".p7").hide();
        $(".p8").hide();
        $(".p9").hide();
        $(".p10").hide();
        $(".p11").hide();
        $(".p12").hide();
        $(".p13").hide();
        $(".fade_in_speed").show();
        $(".change_color_product").attr("src", "img/product/08.png");
        $(".rainbow_bg").attr("src", "img/product/bg.png");
        $(".change_color_keep").show();
        $(".vga_rgb_gif").attr("src", "img/product/001_1.gif");
        $(".vga_rgb_gif").removeClass('vga_color_grab');

    });

    $(".btn2").click(function () {
        // $(".color-piceker").show();
        $(".fuck").hide();
        $(".rainbow_es").hide();
        $(".colorwell").show();
        $(".colorwell").removeClass('shite');
        $(".colorwell").removeClass('shite1');
        $(".colorwell").removeClass('shite2');
        $(".colorwell").removeClass('shite3');
        $(".colorwell").removeClass('shite4');
        $(".vga_fan_light").show();
        $(".vga_fan_light").removeClass('shite');
        $(".vga_fan_light").removeClass('shite1');
        $(".vga_fan_light").removeClass('shite3');
        $(".vga_fan_light").removeClass('shite2');
        $(".p1").hide();
        $(".p3").hide();
        $(".p4").hide();
        $(".p0").hide();
        $(".pes").hide();
        $(".p7").hide();
        $(".p2").show();
        $(".p8").hide();
        $(".p9").hide();
        $(".p10").hide();
        $(".p11").hide();
        $(".p12").hide();
        $(".p13").hide();
        $(".fade_in_speed").hide();
        $('.change_color_product').attr("src", "img/product/08.png");
        $('.rainbow_bg').attr("src", "img/product/bg.png");
        $(".change_color_keep").show();
        $(".vga_rgb_gif").attr("src", "img/product/001_1.gif");
        $(".vga_rgb_gif").removeClass('vga_color_grab');
    });

    $(".btn5").click(function () {
        // $(".color-piceker").show();
        $(".fuck").hide();
        $(".rainbow_es").hide();
        $(".colorwell").show();
        $(".colorwell").removeClass('shite');
        $(".colorwell").removeClass('shite1');
        $(".colorwell").removeClass('shite3');
        $(".colorwell").removeClass('shite4');
        $(".colorwell").addClass('shite2');
        $(".vga_fan_light").show();
        $(".vga_fan_light").removeClass('shite1');
        $(".vga_fan_light").addClass('shite2');
        $(".vga_fan_light").removeClass('shite3');
        $(".vga_fan_light").removeClass('shite');

        $(".p0").hide();
        $(".p1").hide();
        $(".p2").hide();
        $(".p4").hide();
        $(".pes").hide();
        $(".p3").show();
        $(".p7").hide();
        $(".p8").hide();
        $(".p9").hide();
        $(".p10").hide();
        $(".p11").hide();
        $(".p12").hide();
        $(".p13").hide();
        $(".fade_in_speed").hide();
        $('.change_color_product').attr("src", "img/product/08.png");
        $('.rainbow_bg').attr("src", "img/product/bg.png");
        $(".change_color_keep").show();
        $(".vga_rgb_gif").attr("src", "img/product/001_1.gif");
        $(".vga_rgb_gif").removeClass('vga_color_grab');
    });


    $(".btn6").click(function () {
        // $(".color-piceker").show();
        $(".fuck").hide();
        $(".rainbow_es").hide();
        $(".colorwell").show();
        $(".colorwell").removeClass('shite');
        $(".colorwell").removeClass('shite1');
        $(".colorwell").removeClass('shite2');
        $(".colorwell").removeClass('shite4');
        $(".colorwell").addClass('shite3');
        $(".vga_fan_light").show();
        $(".vga_fan_light").removeClass('shite2');
        $(".vga_fan_light").removeClass('shite1');
        $(".vga_fan_light").removeClass('shite');
        $(".vga_fan_light").addClass('shite3');
        $(".p0").hide();
        $(".p1").hide();
        $(".p2").hide();
        $(".p3").hide();
        $(".pes").hide();
        $(".p4").show();
        $(".p7").hide();
        $(".p8").hide();
        $(".p9").hide();
        $(".p10").hide();
        $(".p11").hide();
        $(".p12").hide();
        $(".p13").hide();
        $(".fade_in_speed").hide();
        $('.change_color_product').attr("src", "img/product/08.png");
        $('.rainbow_bg').attr("src", "img/product/bg.png");
        $(".change_color_keep").show();
        $(".vga_rgb_gif").attr("src", "img/product/001_1.gif");
        $(".vga_rgb_gif").removeClass('vga_color_grab');
    });

    $(".btn7").click(function () {
        // $(".color-piceker").hide();
        $(".fuck").hide();
        $(".rainbow_es").hide();
        $(".colorwell").show();
        $(".colorwell").removeClass('shite');
        $(".colorwell").removeClass('shite1');
        $(".colorwell").removeClass('shite2');
        $(".colorwell").removeClass('shite3');
        $(".colorwell").addClass('shite4');
        $(".vga_fan_light").show();
        $(".vga_fan_light").removeClass('shite');
        $(".vga_fan_light").removeClass('shite1');
        $(".vga_fan_light").removeClass('shite2');
        $(".vga_fan_light").removeClass('shite3');
        $(".p0").hide();
        $(".p1").hide();
        $(".p2").hide();
        $(".p3").hide();
        $(".pes").hide();
        $(".p4").hide();
        $(".p7").show();
        $(".p8").hide();
        $(".p9").hide();
        $(".p10").hide();
        $(".p11").hide();
        $(".p12").hide();
        $(".p13").hide();
        $(".fade_in_speed").hide();
        $('.change_color_product').attr("src", "img/product/08.png");
        $('.rainbow_bg').attr("src", "img/product/bg.png");
        $(".change_color_keep").show();
        $(".vga_rgb_gif").attr("src", "img/product/001_1.gif");
        $(".vga_rgb_gif").removeClass('vga_color_grab');
    });

    $(".btn8").click(function () {
        // $(".color-piceker").hide();
        $(".fuck").hide();
        $(".rainbow_es").hide();
        $(".colorwell").show();
        $(".colorwell").removeClass('shite');
        $(".colorwell").removeClass('shite1');
        $(".colorwell").removeClass('shite2');
        $(".colorwell").removeClass('shite3');
        $(".colorwell").addClass('shite4');
        $(".vga_fan_light").removeClass('shite');
        $(".vga_fan_light").removeClass('shite1');
        $(".vga_fan_light").removeClass('shite2');
        $(".vga_fan_light").removeClass('shite3');
        $(".vga_fan_light").show();
        $(".p0").hide();
        $(".p1").hide();
        $(".p2").hide();
        $(".p3").hide();
        $(".pes").hide();
        $(".p4").hide();
        $(".p7").hide();
        $(".p8").show();
        $(".p9").hide();
        $(".p10").hide();
        $(".p11").hide();
        $(".p12").hide();
        $(".p13").hide();
        $(".fade_in_speed").hide();
        $('.change_color_product').attr("src", "img/product/08.png");
        $('.rainbow_bg').attr("src", "img/product/bg.png");
        $(".change_color_keep").hide();
        $(".vga_rgb_gif").attr("src", "img/product/threecolor.gif");
        $(".vga_rgb_gif").removeClass('vga_color_grab');
    });


    $(".btn9").click(function () {
        // $(".color-piceker").hide();
        $(".fuck").hide();
        $(".rainbow_es").hide();
        $(".colorwell").show();
        $(".colorwell").removeClass('shite');
        $(".colorwell").removeClass('shite1');
        $(".colorwell").removeClass('shite2');
        $(".colorwell").removeClass('shite3');
        $(".colorwell").addClass('shite4');
        $(".vga_fan_light").hide();
        $(".p0").hide();
        $(".p1").hide();
        $(".p2").hide();
        $(".p3").hide();
        $(".pes").hide();
        $(".p4").hide();
        $(".p7").hide();
        $(".p8").hide();
        $(".p9").show();
        $(".p10").hide();
        $(".p11").hide();
        $(".p12").hide();
        $(".p13").hide();
        $(".fade_in_speed").hide();
        $('.change_color_product').attr("src", "img/product/08.png");
        $('.rainbow_bg').attr("src", "img/product/bg.png");
        $(".change_color_keep").hide();
        $(".vga_rgb_gif").attr("src", "img/product/sevenrotate.gif");
        $(".vga_rgb_gif").removeClass('vga_color_grab');
    });

    $(".btn10").click(function () {
        // $(".color-piceker").hide();
        $(".fuck").hide();
        $(".rainbow_es").hide();
        $(".colorwell").show();
        $(".colorwell").removeClass('shite');
        $(".colorwell").removeClass('shite1');
        $(".colorwell").removeClass('shite2');
        $(".colorwell").removeClass('shite3');
        $(".colorwell").addClass('shite4');
        $(".vga_fan_light").removeClass('shite');
        $(".vga_fan_light").removeClass('shite1');
        $(".vga_fan_light").removeClass('shite2');
        $(".vga_fan_light").removeClass('shite3');
        $(".vga_fan_light").show();
        $(".p0").hide();
        $(".p1").hide();
        $(".p2").hide();
        $(".p3").hide();
        $(".pes").hide();
        $(".p4").hide();
        $(".p7").hide();
        $(".p8").hide();
        $(".p9").hide();
        $(".p10").show();
        $(".p11").hide();
        $(".p12").hide();
        $(".p13").hide();
        $(".fade_in_speed").hide();
        $('.change_color_product').attr("src", "img/product/08.png");
        $('.rainbow_bg').attr("src", "img/product/bg.png");
        $(".change_color_keep").hide();
        $(".vga_rgb_gif").attr("src", "img/product/colormove.png");
        $(".vga_rgb_gif").removeClass('vga_color_grab');
    });


    $(".btn11").click(function () {
        // $(".color-piceker").hide();
        $(".fuck").hide();
        $(".rainbow_es").hide();
        $(".colorwell").show();
        $(".colorwell").removeClass('shite');
        $(".colorwell").removeClass('shite1');
        $(".colorwell").removeClass('shite2');
        $(".colorwell").removeClass('shite3');
        $(".colorwell").addClass('shite4');
        $(".vga_fan_light").hide();
        $(".p0").hide();
        $(".p1").hide();
        $(".p2").hide();
        $(".p3").hide();
        $(".pes").hide();
        $(".p4").hide();
        $(".p7").hide();
        $(".p8").hide();
        $(".p9").hide();
        $(".p10").hide();
        $(".p11").show();
        $(".p12").hide();
        $(".p13").hide();
        $(".fade_in_speed").hide();
        $('.change_color_product').attr("src", "img/product/08.png");
        $('.rainbow_bg').attr("src", "img/product/bg.png");
        $(".change_color_keep").hide();
        $(".vga_rgb_gif").attr("src", "img/product/seemcolor.gif");
        $(".vga_rgb_gif").removeClass('vga_color_grab');
    });


    $(".btn12").click(function () {
        // $(".color-piceker").hide();
        $(".fuck").hide();
        $(".rainbow_es").hide();
        $(".colorwell").show();
        $(".colorwell").removeClass('shite');
        $(".colorwell").removeClass('shite1');
        $(".colorwell").removeClass('shite2');
        $(".colorwell").removeClass('shite3');
        $(".colorwell").addClass('shite4');
        $(".vga_fan_light").hide();
        $(".p0").hide();
        $(".p1").hide();
        $(".p2").hide();
        $(".p3").hide();
        $(".pes").hide();
        $(".p4").hide();
        $(".p7").hide();
        $(".p8").hide();
        $(".p9").hide();
        $(".p10").hide();
        $(".p11").hide();
        $(".p12").show();
        $(".p13").hide();
        $(".fade_in_speed").hide();
        $('.change_color_product').attr("src", "img/product/08.png");
        $('.rainbow_bg').attr("src", "img/product/bg.png");
        $(".change_color_keep").hide();
        $(".vga_rgb_gif").attr("src", "img/product/threegrab.gif");
        $(".vga_rgb_gif").addClass('vga_color_grab');
    });

    $(".btn13").click(function () {
        // $(".color-piceker").hide();
        $(".fuck").hide();
        $(".rainbow_es").hide();
        $(".colorwell").show();
        $(".colorwell").removeClass('shite');
        $(".colorwell").removeClass('shite1');
        $(".colorwell").removeClass('shite2');
        $(".colorwell").removeClass('shite3');
        $(".colorwell").addClass('shite4');
        $(".vga_fan_light").hide();
        $(".p0").hide();
        $(".p1").hide();
        $(".p2").hide();
        $(".p3").hide();
        $(".pes").hide();
        $(".p4").hide();
        $(".p7").hide();
        $(".p8").hide();
        $(".p9").hide();
        $(".p10").hide();
        $(".p11").hide();
        $(".p12").hide();
        $(".p13").show();
        $(".fade_in_speed").hide();
        $('.change_color_product').attr("src", "img/product/08.png");
        $('.rainbow_bg').attr("src", "img/product/bg.png");
        $(".change_color_keep").hide();
        $(".vga_rgb_gif").attr("src", "img/product/ed.gif");
        $(".vga_rgb_gif").removeClass('vga_color_grab');

    });


    // rainbow change COLOR

    $(".btn0").click(function () {
        // $(".color-piceker").hide();
        $(".colorwell").hide();
        $(".rainbow_es").hide();
        $(".fuck").show();
        $(".vga_fan_light").show();
        $(".p1").hide();
        $(".p2").hide();
        $(".p3").hide();
        $(".pes").hide();
        $(".p4").hide();
        $(".p0").show();
        $(".p7").hide();
        $(".p8").hide();
        $(".p9").hide();
        $(".p10").hide();
        $(".p11").hide();
        $(".p12").hide();
        $(".p13").hide();
        $(".fade_in_speed").hide();
        $('.change_color_product').attr("src", "img/product/08_color.png");
        $('.rainbow_bg').attr("src", "img/product/bg_double.png");
        $(".change_color_keep").hide();
        $(".vga_rgb_gif").attr("src", "img/product/001_1.gif");
        $(".vga_rgb_gif").removeClass('vga_color_grab');
    });





    $(".btn3").click(function () {

        $(".fuck").hide();
        $(".rainbow_es").hide();
        $(".colorwell").show();
        $(".colorwell").removeClass('shite');
        $(".colorwell").removeClass('shite2');
        $(".colorwell").removeClass('shite3');
        $(".colorwell").addClass('shite1');
        $(".btn4").removeClass('active');
        $(".btn3").addClass('active');
        $(".vga_fan_light").removeClass('shite');
        $(".vga_fan_light").removeClass('shite2');
        $(".vga_fan_light").removeClass('shite3');
        $(".vga_fan_light").addClass('shite1');


    });

    $(".btn4").click(function () {
        $(".fuck").hide();
        $(".rainbow_es").hide();
        $(".colorwell").show();
        $(".colorwell").removeClass('shite1');
        $(".colorwell").removeClass('shite2');
        $(".colorwell").removeClass('shite3');
        $(".colorwell").addClass('shite');
        $(".btn3").removeClass('active');
        $(".btn4").addClass('active');
        $(".vga_fan_light").removeClass('shite1');
        $(".vga_fan_light").removeClass('shite2');
        $(".vga_fan_light").removeClass('shite3');
        $(".vga_fan_light").addClass('shite');
    });




    $(".btn0").click(function () {
        $("#effect_cycling").attr("src", "img/pic/0006_1.png");
        $("#effect_content").attr("src", "img/pic/0003.png");
        $("#effect_flash").attr("src", "img/pic/0004.png");
        $("#effect_dualflash").attr("src", "img/pic/0005.png");
        $("#effect_breating").attr("src", "img/pic/0001.png");
        $("#effect_rotate").attr("src", "img/pic/0002.png");
        $("#effect_3color_cycling").attr("src", "img/pic/0008.png");
        $("#effect_7color_cycling").attr("src", "img/pic/0009.png");
        $("#effect_color_moving").attr("src", "img/pic/0010.png");
        $("#effect_gradation_cycling").attr("src", "img/pic/0011.png");
        $("#effect_3grab").attr("src", "img/pic/0012.png");
        $("#effect_ed").attr("src", "img/pic/0013.png");

    });

    $(".btn2").click(function () {
        $("#effect_cycling").attr("src", "img/pic/0006.png");
        $("#effect_content").attr("src", "img/pic/0003_1.png");
        $("#effect_flash").attr("src", "img/pic/0004.png");
        $("#effect_dualflash").attr("src", "img/pic/0005.png");
        $("#effect_breating").attr("src", "img/pic/0001.png");
        $("#effect_rotate").attr("src", "img/pic/0002.png");
        $("#effect_3color_cycling").attr("src", "img/pic/0008.png");
        $("#effect_7color_cycling").attr("src", "img/pic/0009.png");
        $("#effect_color_moving").attr("src", "img/pic/0010.png");
        $("#effect_gradation_cycling").attr("src", "img/pic/0011.png");
        $("#effect_3grab").attr("src", "img/pic/0012.png");
        $("#effect_ed").attr("src", "img/pic/0013.png");

    });

    $(".btn5").click(function () {
        $("#effect_cycling").attr("src", "img/pic/0006.png");
        $("#effect_content").attr("src", "img/pic/0003.png");
        $("#effect_flash").attr("src", "img/pic/0004_1.png");
        $("#effect_dualflash").attr("src", "img/pic/0005.png");
        $("#effect_breating").attr("src", "img/pic/0001.png");
        $("#effect_rotate").attr("src", "img/pic/0002.png");
        $("#effect_3color_cycling").attr("src", "img/pic/0008.png");
        $("#effect_7color_cycling").attr("src", "img/pic/0009.png");
        $("#effect_color_moving").attr("src", "img/pic/0010.png");
        $("#effect_gradation_cycling").attr("src", "img/pic/0011.png");
        $("#effect_3grab").attr("src", "img/pic/0012.png");
        $("#effect_ed").attr("src", "img/pic/0013.png");

    });

    $(".btn6").click(function () {
        $("#effect_cycling").attr("src", "img/pic/0006.png");
        $("#effect_content").attr("src", "img/pic/0003.png");
        $("#effect_flash").attr("src", "img/pic/0004.png");
        $("#effect_dualflash").attr("src", "img/pic/0005_1.png");
        $("#effect_breating").attr("src", "img/pic/0001.png");
        $("#effect_rotate").attr("src", "img/pic/0002.png");
        $("#effect_3color_cycling").attr("src", "img/pic/0008.png");
        $("#effect_7color_cycling").attr("src", "img/pic/0009.png");
        $("#effect_color_moving").attr("src", "img/pic/0010.png");
        $("#effect_gradation_cycling").attr("src", "img/pic/0011.png");
        $("#effect_3grab").attr("src", "img/pic/0012.png");
        $("#effect_ed").attr("src", "img/pic/0013.png");

    });

    $(".btn1").click(function () {
        $("#effect_cycling").attr("src", "img/pic/0006.png");
        $("#effect_content").attr("src", "img/pic/0003.png");
        $("#effect_flash").attr("src", "img/pic/0004.png");
        $("#effect_dualflash").attr("src", "img/pic/0005.png");
        $("#effect_breating").attr("src", "img/pic/0001_1.png");
        $("#effect_rotate").attr("src", "img/pic/0002.png");
        $("#effect_3color_cycling").attr("src", "img/pic/0008.png");
        $("#effect_7color_cycling").attr("src", "img/pic/0009.png");
        $("#effect_color_moving").attr("src", "img/pic/0010.png");
        $("#effect_gradation_cycling").attr("src", "img/pic/0011.png");
        $("#effect_3grab").attr("src", "img/pic/0012.png");
        $("#effect_ed").attr("src", "img/pic/0013.png");

    });


    $(".btn7").click(function () {
        $("#effect_cycling").attr("src", "img/pic/0006.png");
        $("#effect_content").attr("src", "img/pic/0003.png");
        $("#effect_flash").attr("src", "img/pic/0004.png");
        $("#effect_dualflash").attr("src", "img/pic/0005.png");
        $("#effect_breating").attr("src", "img/pic/0001.png");
        $("#effect_rotate").attr("src", "img/pic/0002_1.png");
        $("#effect_3color_cycling").attr("src", "img/pic/0008.png");
        $("#effect_7color_cycling").attr("src", "img/pic/0009.png");
        $("#effect_color_moving").attr("src", "img/pic/0010.png");
        $("#effect_gradation_cycling").attr("src", "img/pic/0011.png");
        $("#effect_3grab").attr("src", "img/pic/0012.png");
        $("#effect_ed").attr("src", "img/pic/0013.png");

    });


    $(".btn8").click(function () {
        $("#effect_cycling").attr("src", "img/pic/0006.png");
        $("#effect_content").attr("src", "img/pic/0003.png");
        $("#effect_flash").attr("src", "img/pic/0004.png");
        $("#effect_dualflash").attr("src", "img/pic/0005.png");
        $("#effect_breating").attr("src", "img/pic/0001.png");
        $("#effect_rotate").attr("src", "img/pic/0002.png");
        $("#effect_3color_cycling").attr("src", "img/pic/0008_1.png");
        $("#effect_7color_cycling").attr("src", "img/pic/0009.png");
        $("#effect_color_moving").attr("src", "img/pic/0010.png");
        $("#effect_gradation_cycling").attr("src", "img/pic/0011.png");
        $("#effect_3grab").attr("src", "img/pic/0012.png");
        $("#effect_ed").attr("src", "img/pic/0013.png");

    });


    $(".btn9").click(function () {
        $("#effect_cycling").attr("src", "img/pic/0006.png");
        $("#effect_content").attr("src", "img/pic/0003.png");
        $("#effect_flash").attr("src", "img/pic/0004.png");
        $("#effect_dualflash").attr("src", "img/pic/0005.png");
        $("#effect_breating").attr("src", "img/pic/0001.png");
        $("#effect_rotate").attr("src", "img/pic/0002.png");
        $("#effect_3color_cycling").attr("src", "img/pic/0008.png");
        $("#effect_7color_cycling").attr("src", "img/pic/0009_1.png");
        $("#effect_color_moving").attr("src", "img/pic/0010.png");
        $("#effect_gradation_cycling").attr("src", "img/pic/0011.png");
        $("#effect_3grab").attr("src", "img/pic/0012.png");
        $("#effect_ed").attr("src", "img/pic/0013.png");

    });


    $(".btn10").click(function () {
        $("#effect_cycling").attr("src", "img/pic/0006.png");
        $("#effect_content").attr("src", "img/pic/0003.png");
        $("#effect_flash").attr("src", "img/pic/0004.png");
        $("#effect_dualflash").attr("src", "img/pic/0005.png");
        $("#effect_breating").attr("src", "img/pic/0001.png");
        $("#effect_rotate").attr("src", "img/pic/0002.png");
        $("#effect_3color_cycling").attr("src", "img/pic/0008.png");
        $("#effect_7color_cycling").attr("src", "img/pic/0009.png");
        $("#effect_color_moving").attr("src", "img/pic/0010_1.png");
        $("#effect_gradation_cycling").attr("src", "img/pic/0011.png");
        $("#effect_3grab").attr("src", "img/pic/0012.png");
        $("#effect_ed").attr("src", "img/pic/0013.png");

    });

    $(".btn11").click(function () {
        $("#effect_cycling").attr("src", "img/pic/0006.png");
        $("#effect_content").attr("src", "img/pic/0003.png");
        $("#effect_flash").attr("src", "img/pic/0004.png");
        $("#effect_dualflash").attr("src", "img/pic/0005.png");
        $("#effect_breating").attr("src", "img/pic/0001.png");
        $("#effect_rotate").attr("src", "img/pic/0002.png");
        $("#effect_3color_cycling").attr("src", "img/pic/0008.png");
        $("#effect_7color_cycling").attr("src", "img/pic/0009.png");
        $("#effect_color_moving").attr("src", "img/pic/0010.png");
        $("#effect_gradation_cycling").attr("src", "img/pic/0011_1.png");
        $("#effect_3grab").attr("src", "img/pic/0012.png");
        $("#effect_ed").attr("src", "img/pic/0013.png");

    });

    $(".btn12").click(function () {
        $("#effect_cycling").attr("src", "img/pic/0006.png");
        $("#effect_content").attr("src", "img/pic/0003.png");
        $("#effect_flash").attr("src", "img/pic/0004.png");
        $("#effect_dualflash").attr("src", "img/pic/0005.png");
        $("#effect_breating").attr("src", "img/pic/0001.png");
        $("#effect_rotate").attr("src", "img/pic/0002.png");
        $("#effect_3color_cycling").attr("src", "img/pic/0008.png");
        $("#effect_7color_cycling").attr("src", "img/pic/0009.png");
        $("#effect_color_moving").attr("src", "img/pic/0010.png");
        $("#effect_gradation_cycling").attr("src", "img/pic/0011.png");
        $("#effect_3grab").attr("src", "img/pic/0012_1.png");
        $("#effect_ed").attr("src", "img/pic/0013.png");

    });


    $(".btn13").click(function () {
        $("#effect_cycling").attr("src", "img/pic/0006.png");
        $("#effect_content").attr("src", "img/pic/0003.png");
        $("#effect_flash").attr("src", "img/pic/0004.png");
        $("#effect_dualflash").attr("src", "img/pic/0005.png");
        $("#effect_breating").attr("src", "img/pic/0001.png");
        $("#effect_rotate").attr("src", "img/pic/0002.png");
        $("#effect_3color_cycling").attr("src", "img/pic/0008.png");
        $("#effect_7color_cycling").attr("src", "img/pic/0009.png");
        $("#effect_color_moving").attr("src", "img/pic/0010.png");
        $("#effect_gradation_cycling").attr("src", "img/pic/0011.png");
        $("#effect_3grab").attr("src", "img/pic/0012.png");
        $("#effect_ed").attr("src", "img/pic/0013_1.png");

    });






});