		// $('.monitor_content_1').show();
		// $('.monitor_content_2').hide();
		// $('.monitor_content_3').hide();
		// $('.monitor_content_4').hide();
		// $('.monitor_content_5').hide();

		$('.monitor_degree').hide();
		$('.ba-slider').hide();
		$('.monitor_part2_inside_gameassist').hide();
		$('.voice_bg').hide();


		$('.monitor_button').click(function() {
		    $('.monitor_content_1').removeClass('monitor_content_1_ani');
		    $('.monitor_content_2').removeClass('monitor_content_2_ani');
		    $('.monitor_content_3').removeClass('monitor_content_3_ani');
		    $('.monitor_content_4').removeClass('monitor_content_4_ani');
		    $('.monitor_content_5').removeClass('monitor_content_5_ani');
		    $('.monitor_button_img_1').addClass('monitor_button_click');
		    $('.monitor_button_img_2').addClass('monitor_button_click');
		    $('.monitor_button_img_3').addClass('monitor_button_click');
		    $('.monitor_button_img_4').addClass('monitor_button_click');
		    $('.monitor_button_img_5').addClass('monitor_button_click');
		    $('.monitor_button_img_1').removeClass('monitor_button_img_1_ani');
		    $('.monitor_button_img_2').removeClass('monitor_button_img_2_ani');
		    $('.monitor_button_img_3').removeClass('monitor_button_img_3_ani');
		    $('.monitor_button_img_4').removeClass('monitor_button_img_4_ani');
		    $('.monitor_button_img_5').removeClass('monitor_button_img_5_ani');
		    $('.monitor_part1_bg_1').removeClass('monitor_part1_bg_1_ani');
		    $('.monitor_part1_bg_2').removeClass('monitor_part1_bg_2_ani');
		    $('.monitor_part1_bg_3').removeClass('monitor_part1_bg_3_ani');
		    $('.monitor_part1_bg_4').removeClass('monitor_part1_bg_4_ani');
		    $('.monitor_part1_bg_5').removeClass('monitor_part1_bg_5_ani');
		    $('.monitor_part1_inside_1').removeClass('monitor_part1_inside_1_ani');
		    $('.monitor_part1_inside_2').removeClass('monitor_part1_inside_2_ani');
		    $('.monitor_part1_inside_3').removeClass('monitor_part1_inside_3_ani');
		    $('.monitor_part1_inside_4').removeClass('monitor_part1_inside_4_ani');
		    $('.monitor_part1_inside_5').removeClass('monitor_part1_inside_5_ani');
		    $('.handle').removeClass('handle_0');
		    $('.handle2').removeClass('handle_0');
		    $('.resize').removeClass('resize_0');
		    $('.handle').css('left', '57%');
		    $('.handle2').css('left', '57%');
		    $('.resize').css('width', '57%');
		    $('.resize').css('left', '0');
		});


		// part1

		$('.monitor_button_1').click(function() {
		    $('.monitor_content_1').show();
		    $('.monitor_content_2').hide();
		    $('.monitor_content_3').hide();
		    $('.monitor_content_4').hide();
		    $('.monitor_content_5').hide();
		    $('.monitor_degree').hide();
		    $('.monitor_button_img_1').addClass('monitor_button_active');
		    $('.monitor_button_img_2').removeClass('monitor_button_active');
		    $('.monitor_button_img_3').removeClass('monitor_button_active');
		    $('.monitor_button_img_4').removeClass('monitor_button_active');
		    $('.monitor_button_img_5').removeClass('monitor_button_active');
		    $('.monitor_part1_bg_1').css('opacity', '1');
		    $('.monitor_part1_bg_1').siblings().not('div').css('opacity', '0');
		    $('.monitor_part1_inside_1').css('opacity', '1');
		    $('.monitor_part1_inside_1').siblings().not('.frame').css('opacity', '0');
		    $('.ba-slider').show();
		    $('.ba-slider1_img1').attr("src", "/public/image/monitor/logo/25_c.png");
		    $('.ba-slider1_img2').attr("src", "/public/image/monitor/logo/25.png");
		    $('.handle').addClass('handle_0');
		    $('.resize').addClass('resize_0');

		});


		$('.monitor_button_2').click(function() {
		    $('.monitor_content_1').hide();
		    $('.monitor_content_2').show();
		    $('.monitor_content_3').hide();
		    $('.monitor_content_4').hide();
		    $('.monitor_content_5').hide();
		    $('.monitor_degree').hide();
		    $('.monitor_button_img_1').removeClass('monitor_button_active');
		    $('.monitor_button_img_2').addClass('monitor_button_active');
		    $('.monitor_button_img_3').removeClass('monitor_button_active');
		    $('.monitor_button_img_4').removeClass('monitor_button_active');
		    $('.monitor_button_img_5').removeClass('monitor_button_active');
		    $('.monitor_part1_bg_2').css('opacity', '1');
		    $('.monitor_part1_bg_2').siblings().not('div').css('opacity', '0');
		    $('.monitor_part1_inside_2').css('opacity', '1');
		    $('.monitor_part1_inside_2').siblings().not('.frame').css('opacity', '0');
		    $('.ba-slider').show();
		    $('.ba-slider1_img1').attr("src", "/public/image/monitor/logo/31.png");
		    $('.ba-slider1_img2').attr("src", "/public/image/monitor/logo/31_c.png");
		    $('.handle').addClass('handle_0');
		    $('.resize').addClass('resize_0');
		});


		$('.monitor_button_3').click(function() {
		    $('.monitor_content_1').hide();
		    $('.monitor_content_2').hide();
		    $('.monitor_content_3').show();
		    $('.monitor_content_4').hide();
		    $('.monitor_content_5').hide();
		    $('.monitor_button_img_1').removeClass('monitor_button_active');
		    $('.monitor_button_img_2').removeClass('monitor_button_active');
		    $('.monitor_button_img_3').addClass('monitor_button_active');
		    $('.monitor_button_img_4').removeClass('monitor_button_active');
		    $('.monitor_button_img_5').removeClass('monitor_button_active');
		    $('.monitor_part1_bg_3').css('opacity', '1');
		    $('.monitor_part1_bg_3').siblings().not('div').css('opacity', '0');
		    $('.monitor_part1_inside_3').css('opacity', '1').siblings().not('.monitor_part1_inside_3').not('.frame').css('opacity', '0');
		    $('.ba-slider').hide();
		});


		$('.monitor_button_4').click(function() {
		    $('.monitor_content_1').hide();
		    $('.monitor_content_2').hide();
		    $('.monitor_content_3').hide();
		    $('.monitor_content_4').show();
		    $('.monitor_content_5').hide();
		    $('.monitor_button_img_1').removeClass('monitor_button_active');
		    $('.monitor_button_img_2').removeClass('monitor_button_active');
		    $('.monitor_button_img_3').removeClass('monitor_button_active');
		    $('.monitor_button_img_4').addClass('monitor_button_active');
		    $('.monitor_button_img_5').removeClass('monitor_button_active');
		    $('.monitor_part1_bg_4').css('opacity', '1');
		    $('.monitor_part1_bg_4').siblings().not('div').css('opacity', '0');
		    $('.monitor_part1_inside_4').css('opacity', '1');
		    $('.monitor_part1_inside_4').siblings().not('.frame').css('opacity', '0');
		    $('.ba-slider').hide();
		});


		$('.monitor_button_5').click(function() {
		    $('.monitor_content_1').hide();
		    $('.monitor_content_2').hide();
		    $('.monitor_content_3').hide();
		    $('.monitor_content_4').hide();
		    $('.monitor_content_5').show();
		    $('.monitor_degree').hide();
		    $('.monitor_button_img_1').removeClass('monitor_button_active');
		    $('.monitor_button_img_2').removeClass('monitor_button_active');
		    $('.monitor_button_img_3').removeClass('monitor_button_active');
		    $('.monitor_button_img_4').removeClass('monitor_button_active');
		    $('.monitor_button_img_5').addClass('monitor_button_active');
		    $('.monitor_part1_bg_5').css('opacity', '1');
		    $('.monitor_part1_bg_5').siblings().not('div').css('opacity', '0');
		    $('.monitor_part1_inside_5').css('opacity', '1');
		    $('.monitor_part1_inside_5').siblings().not('.frame').css('opacity', '0');
		    $('.ba-slider').show();
		    $('.ba-slider1_img1').attr("src", "img/monitor/22_c .png");
		    $('.ba-slider1_img2').attr("src", "img/monitor/22.png");
		    $('.handle').addClass('handle_0');
		    $('.resize').addClass('resize_0');
		});


		// part2


		$('.monitor_button2').click(function() {
		    $('.monitor_content2_1').removeClass('monitor_content2_1_ani');
		    $('.monitor_content2_2').removeClass('monitor_content2_2_ani');
		    $('.monitor_content2_3').removeClass('monitor_content2_3_ani');
		    $('.monitor_content2_4').removeClass('monitor_content2_4_ani');
		    $('.monitor_content2_5').removeClass('monitor_content2_5_ani');
		    $('.monitor_content2_6').removeClass('monitor_content2_6_ani');
		    $('.monitor_content2_7').removeClass('monitor_content2_7_ani');
		    $('.monitor_button2_img_1').addClass('monitor_button_click');
		    $('.monitor_button2_img_2').addClass('monitor_button_click');
		    $('.monitor_button2_img_3').addClass('monitor_button_click');
		    $('.monitor_button2_img_4').addClass('monitor_button_click');
		    $('.monitor_button2_img_5').addClass('monitor_button_click');
		    $('.monitor_button2_img_6').addClass('monitor_button_click');
		    $('.monitor_button2_img_7').addClass('monitor_button_click');
		    $('.monitor_button2_img_1').removeClass('monitor_button2_img_1_ani');
		    $('.monitor_button2_img_2').removeClass('monitor_button2_img_2_ani');
		    $('.monitor_button2_img_3').removeClass('monitor_button2_img_3_ani');
		    $('.monitor_button2_img_4').removeClass('monitor_button2_img_4_ani');
		    $('.monitor_button2_img_5').removeClass('monitor_button2_img_5_ani');
		    $('.monitor_button2_img_6').removeClass('monitor_button2_img_6_ani');
		    $('.monitor_button2_img_7').removeClass('monitor_button2_img_7_ani');
		    $('.monitor_part2_bg_1').removeClass('monitor_part2_bg_1_ani');
		    $('.monitor_part2_bg_2').removeClass('monitor_part2_bg_2_ani');
		    $('.monitor_part2_bg_3').removeClass('monitor_part2_bg_3_ani');
		    $('.monitor_part2_bg_4').removeClass('monitor_part2_bg_4_ani');
		    $('.monitor_part2_bg_5').removeClass('monitor_part2_bg_5_ani');
		    $('.monitor_part2_bg_6').removeClass('monitor_part2_bg_6_ani');
		    $('.monitor_part2_bg_7').removeClass('monitor_part2_bg_7_ani');
		    $('.monitor_part2_inside_1').removeClass('monitor_part2_inside_1_ani');
		    $('.monitor_part2_inside_2').removeClass('monitor_part2_inside_2_ani');
		    $('.monitor_part2_inside_3').removeClass('monitor_part2_inside_3_ani');
		    $('.monitor_part2_inside_4').removeClass('monitor_part2_inside_4_ani');
		    $('.monitor_part2_inside_5').removeClass('monitor_part2_inside_5_ani');
		    $('.monitor_part2_inside_6').removeClass('monitor_part2_inside_6_ani');
		    $('.monitor_part2_inside_7').removeClass('monitor_part2_inside_7_ani');
		    $('.monitor_part2_inside_video').removeClass('monitor_part2_inside_video_ani');
		    $(".ANC_video").css("display", "none");
		    $('.ANC_play_icon').css("display", "inline-block");
		    $('.ANC_close_icon').css("display", "none");
		    $('.iframe_video')[0].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
		});



		$('.monitor_button2_1').click(function() {
		    $('.monitor_content2_1').show();
		    $('.monitor_content2_2').hide();
		    $('.monitor_content2_3').hide();
		    $('.monitor_content2_4').hide();
		    $('.monitor_content2_5').hide();
		    $('.monitor_content2_6').hide();
		    $('.monitor_content2_7').hide();
		    $('.voice_bg').hide();
		    $('.monitor_part2_inside_video').show();
		    $('.monitor_part2_inside_gameassist').hide();
		    $('.monitor_button2_img_1').addClass('monitor_button_active');
		    $('.monitor_button2_img_2').removeClass('monitor_button_active');
		    $('.monitor_button2_img_3').removeClass('monitor_button_active');
		    $('.monitor_button2_img_4').removeClass('monitor_button_active');
		    $('.monitor_button2_img_5').removeClass('monitor_button_active');
		    $('.monitor_button2_img_6').removeClass('monitor_button_active');
		    $('.monitor_button2_img_7').removeClass('monitor_button_active');
		    $('.monitor_part2_bg_1').css('opacity', '1').siblings().not('div').css('opacity', '0');
		    $('.monitor_part2_inside_1').css('opacity', '1').siblings().not('.frame2').not('.monitor_part2_inside_video').css('opacity', '0');
		    $('.monitor_part2_inside_video').css('opacity', '1');
		    $(".ANC_video").css("display", "none");
		    $('.ANC_play_icon').css("display", "inline-block");
		    $('.ANC_close_icon').css("display", "none");
		});


		$('.monitor_button2_2').click(function() {
		    $('.monitor_content2_1').hide();
		    $('.monitor_content2_2').show();
		    $('.monitor_content2_3').hide();
		    $('.monitor_content2_4').hide();
		    $('.monitor_content2_5').hide();
		    $('.monitor_content2_6').hide();
		    $('.monitor_content2_7').hide();
		    $('.voice_bg').hide();
		    $('.monitor_part2_inside_video').hide();
		    $('.monitor_part2_inside_gameassist').hide();
		    $('.monitor_button2_img_1').removeClass('monitor_button_active');
		    $('.monitor_button2_img_2').addClass('monitor_button_active');
		    $('.monitor_button2_img_3').removeClass('monitor_button_active');
		    $('.monitor_button2_img_4').removeClass('monitor_button_active');
		    $('.monitor_button2_img_5').removeClass('monitor_button_active');
		    $('.monitor_button2_img_6').removeClass('monitor_button_active');
		    $('.monitor_button2_img_7').removeClass('monitor_button_active');
		    $('.monitor_part2_bg_2').css('opacity', '1').siblings().not('div').css('opacity', '0');
		    $('.monitor_part2_inside_2').css('opacity', '1').siblings().not('.frame2').css('opacity', '0');
		    $(".ANC_video").css("display", "none");
		    $('.ANC_play_icon').css("display", "inline-block");
		    $('.ANC_close_icon').css("display", "none");
		});


		$('.monitor_button2_3').click(function() {
		    $('.monitor_content2_1').hide();
		    $('.monitor_content2_2').hide();
		    $('.monitor_content2_3').show();
		    $('.monitor_content2_4').hide();
		    $('.monitor_content2_5').hide();
		    $('.monitor_content2_6').hide();
		    $('.monitor_content2_7').hide();
		    $('.voice_bg').hide();
		    $('.monitor_part2_inside_video').hide();
		    $('.monitor_part2_inside_gameassist').hide();
		    $('.monitor_button2_img_1').removeClass('monitor_button_active');
		    $('.monitor_button2_img_2').removeClass('monitor_button_active');
		    $('.monitor_button2_img_3').addClass('monitor_button_active');
		    $('.monitor_button2_img_4').removeClass('monitor_button_active');
		    $('.monitor_button2_img_5').removeClass('monitor_button_active');
		    $('.monitor_button2_img_6').removeClass('monitor_button_active');
		    $('.monitor_button2_img_7').removeClass('monitor_button_active');
		    $('.monitor_part2_bg_3').css('opacity', '1').siblings().not('div').css('opacity', '0');
		    $('.monitor_part2_inside_3').css('opacity', '1').siblings().not('.frame2').css('opacity', '0');
		    $(".ANC_video").css("display", "none");
		    $('.ANC_play_icon').css("display", "inline-block");
		    $('.ANC_close_icon').css("display", "none");
		});


		$('.monitor_button2_4').click(function() {
		    $('.monitor_content2_1').hide();
		    $('.monitor_content2_2').hide();
		    $('.monitor_content2_3').hide();
		    $('.monitor_content2_4').show();
		    $('.monitor_content2_5').hide();
		    $('.monitor_content2_6').hide();
		    $('.monitor_content2_7').hide();
		    $('.voice_bg').hide();
		    $('.monitor_part2_inside_video').hide();
		    $('.monitor_button2_img_1').removeClass('monitor_button_active');
		    $('.monitor_button2_img_2').removeClass('monitor_button_active');
		    $('.monitor_button2_img_3').removeClass('monitor_button_active');
		    $('.monitor_button2_img_4').addClass('monitor_button_active');
		    $('.monitor_button2_img_5').removeClass('monitor_button_active');
		    $('.monitor_button2_img_6').removeClass('monitor_button_active');
		    $('.monitor_button2_img_7').removeClass('monitor_button_active');
		    $('.monitor_part2_bg_4').css('opacity', '1').siblings().not('div').css('opacity', '0');
		    $('.monitor_part2_inside_4').css('opacity', '1').siblings().not('.frame2').css('opacity', '0');
		    $(".ANC_video").css("display", "none");
		    $('.ANC_play_icon').css("display", "inline-block");
		    $('.ANC_close_icon').css("display", "none");
		    $('.monitor_part2_inside_gameassist').show().css('opacity', '1');
		});


		$('.monitor_button2_5').click(function() {
		    $('.monitor_content2_1').hide();
		    $('.monitor_content2_2').hide();
		    $('.monitor_content2_3').hide();
		    $('.monitor_content2_4').hide();
		    $('.monitor_content2_5').show();
		    $('.monitor_content2_6').hide();
		    $('.monitor_content2_7').hide();
		    $('.voice_bg').hide();
		    $('.monitor_part2_inside_video').hide();
		    $('.monitor_part2_inside_gameassist').hide();
		    $('.monitor_button2_img_1').removeClass('monitor_button_active');
		    $('.monitor_button2_img_2').removeClass('monitor_button_active');
		    $('.monitor_button2_img_3').removeClass('monitor_button_active');
		    $('.monitor_button2_img_4').removeClass('monitor_button_active');
		    $('.monitor_button2_img_5').addClass('monitor_button_active');
		    $('.monitor_button2_img_6').removeClass('monitor_button_active');
		    $('.monitor_button2_img_7').removeClass('monitor_button_active');
		    $('.monitor_part2_bg_5').css('opacity', '1').siblings().not('div').css('opacity', '0');
		    $('.monitor_part2_inside_5').css('opacity', '1').siblings().not('.frame2').css('opacity', '0');
		    $(".ANC_video").css("display", "none");
		    $('.ANC_play_icon').css("display", "inline-block");
		    $('.ANC_close_icon').css("display", "none");
		});



		$('.monitor_button2_6').click(function() {
		    $('.monitor_content2_1').hide();
		    $('.monitor_content2_2').hide();
		    $('.monitor_content2_3').hide();
		    $('.monitor_content2_4').hide();
		    $('.monitor_content2_5').hide();
		    $('.monitor_content2_6').show();
		    $('.monitor_content2_7').hide();
		    $('.monitor_part2_inside_video').hide();
		    $('.voice_bg').show();
		    $('.monitor_part2_inside_gameassist').hide();
		    $('.monitor_button2_img_1').removeClass('monitor_button_active');
		    $('.monitor_button2_img_2').removeClass('monitor_button_active');
		    $('.monitor_button2_img_3').removeClass('monitor_button_active');
		    $('.monitor_button2_img_4').removeClass('monitor_button_active');
		    $('.monitor_button2_img_5').removeClass('monitor_button_active');
		    $('.monitor_button2_img_6').addClass('monitor_button_active');
		    $('.monitor_button2_img_7').removeClass('monitor_button_active');
		    $('.monitor_part2_bg_6').css('opacity', '1').siblings().not('div').css('opacity', '0');
		    $('.monitor_part2_inside_6').css('opacity', '1').siblings().not('.frame2').css('opacity', '0');
		    $(".ANC_video").css("display", "none");
		    $('.ANC_play_icon').css("display", "inline-block");
		    $('.ANC_close_icon').css("display", "none");
		});



		$('.monitor_button2_7').click(function() {
		    $('.monitor_content2_1').hide();
		    $('.monitor_content2_2').hide();
		    $('.monitor_content2_3').hide();
		    $('.monitor_content2_4').hide();
		    $('.monitor_content2_5').hide();
		    $('.monitor_content2_6').hide();
		    $('.monitor_content2_7').show();
		    $('.voice_bg').hide();
		    $('.monitor_part2_inside_video').hide();
		    $('.monitor_part2_inside_gameassist').hide();
		    $('.monitor_button2_img_1').removeClass('monitor_button_active');
		    $('.monitor_button2_img_2').removeClass('monitor_button_active');
		    $('.monitor_button2_img_3').removeClass('monitor_button_active');
		    $('.monitor_button2_img_4').removeClass('monitor_button_active');
		    $('.monitor_button2_img_5').removeClass('monitor_button_active');
		    $('.monitor_button2_img_6').removeClass('monitor_button_active');
		    $('.monitor_button2_img_7').addClass('monitor_button_active');
		    $('.monitor_part2_bg_7').css('opacity', '1').siblings().not('div').css('opacity', '0');
		    $('.monitor_part2_inside_7').css('opacity', '1').siblings().not('.frame2').css('opacity', '0');
		    $(".ANC_video").css("display", "none");
		    $('.ANC_play_icon').css("display", "inline-block");
		    $('.ANC_close_icon').css("display", "none");
		});

		$(".ANC_video_play").click(function() {
		    $('.monitor_content2_1').hide();
		    $('.monitor_content2_2').hide();
		    $('.monitor_content2_3').hide();
		    $('.monitor_content2_4').hide();
		    $('.monitor_content2_5').hide();
		    $('.monitor_content2_6').show();
		    $('.monitor_content2_7').hide();
		    $('.monitor_part2_inside_video').hide();
		    $('.voice_bg').show();
		    $('.monitor_part2_inside_gameassist').hide();
		    $('.monitor_button2_img_1').removeClass('monitor_button_active');
		    $('.monitor_button2_img_2').removeClass('monitor_button_active');
		    $('.monitor_button2_img_3').removeClass('monitor_button_active');
		    $('.monitor_button2_img_4').removeClass('monitor_button_active');
		    $('.monitor_button2_img_5').removeClass('monitor_button_active');
		    $('.monitor_button2_img_6').addClass('monitor_button_active');
		    $('.monitor_button2_img_7').removeClass('monitor_button_active');
		    $('.monitor_part2').css('background-image', 'url(' + '/public/image/monitor/logo/32_b.jpg' + ')');
		    $('.monitor_part2_inside').css('background-image', 'url(' + '/public/image/monitor/logo/44.jpg' + ')');
		    $('.monitor_content2_1').removeClass('monitor_content2_1_ani');
		    $('.monitor_content2_2').removeClass('monitor_content2_2_ani');
		    $('.monitor_content2_3').removeClass('monitor_content2_3_ani');
		    $('.monitor_content2_4').removeClass('monitor_content2_4_ani');
		    $('.monitor_content2_5').removeClass('monitor_content2_5_ani');
		    $('.monitor_content2_6').removeClass('monitor_content2_6_ani');
		    $('.monitor_content2_7').removeClass('monitor_content2_7_ani');
		    $('.monitor_button2_img_1').addClass('monitor_button_click');
		    $('.monitor_button2_img_2').addClass('monitor_button_click');
		    $('.monitor_button2_img_3').addClass('monitor_button_click');
		    $('.monitor_button2_img_4').addClass('monitor_button_click');
		    $('.monitor_button2_img_5').addClass('monitor_button_click');
		    $('.monitor_button2_img_6').addClass('monitor_button_click');
		    $('.monitor_button2_img_7').addClass('monitor_button_click');
		    $('.monitor_button2_img_1').removeClass('monitor_button2_img_1_ani');
		    $('.monitor_button2_img_2').removeClass('monitor_button2_img_2_ani');
		    $('.monitor_button2_img_3').removeClass('monitor_button2_img_3_ani');
		    $('.monitor_button2_img_4').removeClass('monitor_button2_img_4_ani');
		    $('.monitor_button2_img_5').removeClass('monitor_button2_img_5_ani');
		    $('.monitor_button2_img_6').removeClass('monitor_button2_img_6_ani');
		    $('.monitor_button2_img_7').removeClass('monitor_button2_img_7_ani');
		    $('.monitor_part2').removeClass('monitor_part2_bg');
		    $('.monitor_part2_inside').removeClass('monitor_part2_inside_bg');
		    $('.monitor_part2_inside_video').removeClass('monitor_part2_inside_video_ani');
		    $(".ANC_video").css("display", "block");
		    $('.ANC_play_icon').css("display", "none");
		    $('.ANC_close_icon').css("display", "inline-block");
		    $('.iframe_video')[0].contentWindow.postMessage('{"event":"command","func":"' + 'playVideo' + '","args":""}', '*');
		});
		$('.ANC_close_icon').click(function() {
		    $(".ANC_video").css("display", "none");
		    $('.ANC_play_icon').css("display", "inline-block");
		    $('.ANC_close_icon').css("display", "none");
		    $('.iframe_video')[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
		});
		$('#toANC').click(function() {
		    $('.monitor_content2_1').hide();
		    $('.monitor_content2_2').hide();
		    $('.monitor_content2_3').hide();
		    $('.monitor_content2_4').hide();
		    $('.monitor_content2_5').hide();
		    $('.monitor_content2_6').show();
		    $('.monitor_content2_7').hide();
		    $('.monitor_part2_bg_1').removeClass('monitor_part2_bg_1_ani');
		    $('.monitor_part2_bg_2').removeClass('monitor_part2_bg_2_ani');
		    $('.monitor_part2_bg_3').removeClass('monitor_part2_bg_3_ani');
		    $('.monitor_part2_bg_4').removeClass('monitor_part2_bg_4_ani');
		    $('.monitor_part2_bg_5').removeClass('monitor_part2_bg_5_ani');
		    $('.monitor_part2_bg_6').removeClass('monitor_part2_bg_6_ani');
		    $('.monitor_part2_bg_7').removeClass('monitor_part2_bg_7_ani');
		    $('.monitor_part2_inside_1').removeClass('monitor_part2_inside_1_ani');
		    $('.monitor_part2_inside_2').removeClass('monitor_part2_inside_2_ani');
		    $('.monitor_part2_inside_3').removeClass('monitor_part2_inside_3_ani');
		    $('.monitor_part2_inside_4').removeClass('monitor_part2_inside_4_ani');
		    $('.monitor_part2_inside_5').removeClass('monitor_part2_inside_5_ani');
		    $('.monitor_part2_inside_6').removeClass('monitor_part2_inside_6_ani');
		    $('.monitor_part2_inside_7').removeClass('monitor_part2_inside_7_ani');
		    $('.monitor_part2_inside_video').hide();
		    $('.voice_bg').show();
		    $('.monitor_part2_inside_gameassist').hide();
		    $('.monitor_button2_img_1').removeClass('monitor_button_active');
		    $('.monitor_button2_img_2').removeClass('monitor_button_active');
		    $('.monitor_button2_img_3').removeClass('monitor_button_active');
		    $('.monitor_button2_img_4').removeClass('monitor_button_active');
		    $('.monitor_button2_img_5').removeClass('monitor_button_active');
		    $('.monitor_button2_img_6').addClass('monitor_button_active');
		    $('.monitor_button2_img_7').removeClass('monitor_button_active');
		    $('.monitor_part2_bg_6').css('opacity', '1').siblings().not('div').css('opacity', '0');
		    $('.monitor_part2_inside_6').css('opacity', '1').siblings().not('.frame2').css('opacity', '0');
		    $(".ANC_video").css("display", "block");
		    $('.monitor_content2_1').removeClass('monitor_content2_1_ani');
		    $('.monitor_content2_2').removeClass('monitor_content2_2_ani');
		    $('.monitor_content2_3').removeClass('monitor_content2_3_ani');
		    $('.monitor_content2_4').removeClass('monitor_content2_4_ani');
		    $('.monitor_content2_5').removeClass('monitor_content2_5_ani');
		    $('.monitor_content2_6').removeClass('monitor_content2_6_ani');
		    $('.monitor_content2_7').removeClass('monitor_content2_7_ani');
		    $('.monitor_button2_img_1').addClass('monitor_button_click');
		    $('.monitor_button2_img_2').addClass('monitor_button_click');
		    $('.monitor_button2_img_3').addClass('monitor_button_click');
		    $('.monitor_button2_img_4').addClass('monitor_button_click');
		    $('.monitor_button2_img_5').addClass('monitor_button_click');
		    $('.monitor_button2_img_6').addClass('monitor_button_click');
		    $('.monitor_button2_img_7').addClass('monitor_button_click');
		    $('.monitor_button2_img_1').removeClass('monitor_button2_img_1_ani');
		    $('.monitor_button2_img_2').removeClass('monitor_button2_img_2_ani');
		    $('.monitor_button2_img_3').removeClass('monitor_button2_img_3_ani');
		    $('.monitor_button2_img_4').removeClass('monitor_button2_img_4_ani');
		    $('.monitor_button2_img_5').removeClass('monitor_button2_img_5_ani');
		    $('.monitor_button2_img_6').removeClass('monitor_button2_img_6_ani');
		    $('.monitor_button2_img_7').removeClass('monitor_button2_img_7_ani');
		    $('.monitor_part2').removeClass('monitor_part2_bg');
		    $('.monitor_part2_inside').removeClass('monitor_part2_inside_bg');
		    $('.monitor_part2_inside_video').removeClass('monitor_part2_inside_video_ani');
		    $('.ANC_close_icon').css("display", "inline-block");
		    $('.ANC_play_icon').css("display", "none");
		    $('.iframe_video')[0].contentWindow.postMessage('{"event":"command","func":"' + 'playVideo' + '","args":""}', '*');
		    window.parent.$("html,body").animate({
		        scrollTop: ($('#test_201804').offset().top) + kf_height - vga_main
		    }, 800);
		})
		$('#toPANEL').click(function() {
		    window.parent.$("html,body").animate({
		        scrollTop: ($('#test_201802').offset().top) + kf_height - vga_main
		    }, 800);
		});
		$('#toOSD').click(function() {
		    // $('.monitor_content2_1').removeClass('monitor_content2_1_ani');
		    // $('.monitor_content2_2').removeClass('monitor_content2_2_ani');
		    // $('.monitor_content2_3').removeClass('monitor_content2_3_ani');
		    // $('.monitor_content2_4').removeClass('monitor_content2_4_ani');
		    // $('.monitor_content2_5').removeClass('monitor_content2_5_ani');
		    // $('.monitor_content2_6').removeClass('monitor_content2_6_ani');
		    // $('.monitor_content2_7').removeClass('monitor_content2_7_ani');
		    // $('.monitor_button2_img_1').addClass('monitor_button_click');
		    // $('.monitor_button2_img_2').addClass('monitor_button_click');
		    // $('.monitor_button2_img_3').addClass('monitor_button_click');
		    // $('.monitor_button2_img_4').addClass('monitor_button_click');
		    // $('.monitor_button2_img_5').addClass('monitor_button_click');
		    // $('.monitor_button2_img_6').addClass('monitor_button_click');
		    // $('.monitor_button2_img_7').addClass('monitor_button_click');
		    // $('.monitor_button2_img_1').removeClass('monitor_button2_img_1_ani');
		    // $('.monitor_button2_img_2').removeClass('monitor_button2_img_2_ani');
		    // $('.monitor_button2_img_3').removeClass('monitor_button2_img_3_ani');
		    // $('.monitor_button2_img_4').removeClass('monitor_button2_img_4_ani');
		    // $('.monitor_button2_img_5').removeClass('monitor_button2_img_5_ani');
		    // $('.monitor_button2_img_6').removeClass('monitor_button2_img_6_ani');
		    // $('.monitor_button2_img_7').removeClass('monitor_button2_img_7_ani');
		    // $('.monitor_part2').removeClass('monitor_part2_bg');
		    // $('.monitor_part2_inside').removeClass('monitor_part2_inside_bg');
		    // $('.monitor_part2_inside_video').removeClass('monitor_part2_inside_video_ani');
		    // $('.monitor_content2_1').hide();
		    // $('.monitor_content2_2').hide();
		    // $('.monitor_content2_3').hide();
		    // $('.monitor_content2_4').hide();
		    // $('.monitor_content2_5').show();
		    // $('.monitor_content2_6').hide();
		    // $('.monitor_content2_7').hide();
		    // $('.voice_bg').hide();
		    // $('.monitor_part2_inside_video').hide();
		    // $('.monitor_part2_inside_gameassist').hide();
		    // $('.monitor_button2_img_1').removeClass('monitor_button_active');
		    // $('.monitor_button2_img_2').removeClass('monitor_button_active');
		    // $('.monitor_button2_img_3').removeClass('monitor_button_active');
		    // $('.monitor_button2_img_4').removeClass('monitor_button_active');
		    // $('.monitor_button2_img_5').addClass('monitor_button_active');
		    // $('.monitor_button2_img_6').removeClass('monitor_button_active');
		    // $('.monitor_button2_img_7').removeClass('monitor_button_active');
		    // $('.monitor_part2').css('background-image', 'url(' + 'img/monitor/13_b.jpg' + ')');
		    // $('.monitor_part2_inside').css('background-image', 'url(' + 'img/monitor/13.jpg' + ')');
		    // $(".ANC_video").css("display", "none");
		    // $('.ANC_play_icon').css("display", "inline-block");
		    // $('.ANC_close_icon').css("display", "none");
		    // $('.iframe_video')[0].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
		    window.parent.$("html,body").animate({
		        scrollTop: ($('#test_201804').offset().top) + kf_height - vga_main
		    }, 800);
		});

		$('#toZero').click(function() {
            window.parent.$("html,body").animate({
                scrollTop: ($('#test_201808').offset().top) + kf_height - vga_main
            }, 800);
        });
		
		$(".hover_change_img_W1").mouseenter(function() {
		    $(this).find('img').attr('src', '/public/image/vga/W1hover.png');
		});
		$(".hover_change_img_W1").mouseleave(function() {
		    $(this).find('img').attr('src', '/public/image/vga/W1.png');
		});

		$(".hover_change_img_W2").mouseenter(function() {
		    $(this).find('img').attr('src', '/public/image/vga/W2hover.png');
		});
		$(".hover_change_img_W2").mouseleave(function() {
		    $(this).find('img').attr('src', '/public/image/vga/W2.png');
		});

		$(".hover_change_img_W3").mouseenter(function() {
		    $(this).find('img').attr('src', '/public/image/vga/W3hover.png');
		});
		$(".hover_change_img_W3").mouseleave(function() {
		    $(this).find('img').attr('src', '/public/image/vga/W3.png');
		});
		$(".readmore").click(function() {
		    $(this).css('display', 'none').prev().removeClass('hide-text');
		});
		$(".show-text").click(function() {
		    $(this).addClass('hide-text').next().css('display', 'block');
		});